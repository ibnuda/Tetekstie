﻿[<AutoOpen>]
module Tetekstie.Common

open System
open Freya.Core

module Async =
  let map f x = async {
    let! v = x in return f v
  }

module Option =
  let fromNullable =
    function
    | null -> None
    | x -> Some x

module Either =
  let Success = Choice1Of2
  let Failure = Choice2Of2

  let (|Success|Failure|) m =
    match m with
    | Choice1Of2 x -> Success x
    | Choice2Of2 x -> Failure x

  let bind f m =
    match m with
    | Success x -> f x
    | Failure x -> Failure x

  let toOption =
    function
    | Success x -> Some x
    | Failure x -> None

module Tuple =
  let map f (x, y) = f x, f y

[<AutoOpen>]
module Utils =
  let randomString =
    let chars = "pyfgcrlaoeuidhtns-qjkxbmwvzPYFGCRLAOEUIDHTNS_QJKXBMWVZ7531902468"
    let charsLen = chars.Length
    let rand = System.Random ()
    fun len ->
      let randomChars = [| for i in 0 .. len -> chars.[rand.Next(charsLen)]|]
      new System.String (randomChars)

  type MaybeBuilder () =
    member __.Bind (m, f) = Option.bind f m
    member __.Return (x) = Some x
    member __.ReturnFrom (x) = x

  let maybe = MaybeBuilder ()

[<AutoOpen>]
module Parsing =
  open System.IO
  open System.Globalization
  open Arachne.Http
  open Chiron
  open Chiron.Operators
  open Freya.Lenses.Http

  let keyValue (s: string) =
    match s.Split ([| '=' |]) with
    | [|k; v|] -> Some (k, v)
    | _ -> None

  let decode = System.Net.WebUtility.UrlDecode

  let toMap (s: string) =
    s.Split ([| '&' |])
    |> Array.choose keyValue
    |> Array.map (Tuple.map decode)
    |> Map.ofArray

  let readStream (x: Stream) =
    use reader = new StreamReader (x)
    reader.ReadToEndAsync ()
    |> Async.AwaitTask

  let query =
    Freya.Lens.get Request.Query_
    |> Freya.map (fun x -> let (Arachne.Uri.Query q) = x in q |> toMap)

  let body =
    Freya.Lens.get Request.Body_
    |> Freya.bind (Freya.fromAsync readStream)

  let form =
    body
    |> Freya.map toMap
    |> Freya.memo

  type StoryForm =
    { Url: string
      EditCode: string
      Content: string }

    static member FromJson (_: StoryForm) =
            fun u e c ->
              { Url = u
                EditCode = e
                Content = c }
        <!> Json.read "url"
        <*> Json.read "editCode"
        <*> Json.read "content"

  let readStory =
    freya {
      let! contentType = Freya.Lens.getPartial Request.Headers.ContentType_
      match contentType with
      | Some (ContentType (MediaType (Type "application", SubType "x-www-form-urlencoded", _))) ->
        let! form = form
        let album =
          maybe {
            let! url = form |> Map.tryFind "url"
            let! editCode = form |> Map.tryFind "editCode"
            let! content = form |> Map.tryFind "content"
            return
              { Url = url
                EditCode = editCode
                Content = content }
          }
        return album
      | Some (ContentType m) when m = MediaType.Json ->
        let! body = body
        return (Json.tryParse body |> Either.bind Json.tryDeserialize |> Either.toOption)
      | _ ->
        return None
    } |> Freya.memo

[<AutoOpen>]
module Representation =
  open System.Text
  open Arachne.Http
  open Chiron
  open Freya.Machine.Extensions.Http

  let inline repJson x =
    Freya.init
      { Data = (Json.serialize >> Json.format >> Encoding.UTF8.GetBytes) x
        Description =
          { Charset = Some Charset.Utf8
            Encodings = None
            MediaType = Some MediaType.Json
            Languages = None } }

  let inline ok fetch name spec =
    freya {
      let cont = Db.getContext ()
      let! fetch = fetch
      let res = fetch cont
      return!
        match spec.MediaTypes with
        | Free -> repJson res
        | Negotiated (m :: _) when m = MediaType.Json -> repJson res
        | _ -> failwith "non json is verbodden"
    }