﻿[<RequireQualifiedAccess>]
module Db

open System
open FSharp.Data.Sql

type Sql =
  SqlDataProvider<
    ConnectionString="Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Tetekstie;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False",
    DatabaseVendor=Common.DatabaseProviderTypes.MSSQLSERVER>

type DbContext = Sql.dataContext
type Story = DbContext.``dbo.StoriesEntity``

let getContext () = Sql.GetDataContext ()
let firstOrNone s = s |> Seq.tryFind (fun _ -> true)

let getStory url (cont: DbContext): Story option =
  query {
    for story in cont.Dbo.Stories do
    where (story.Url = url)
    select story
  } |> firstOrNone

let newStory (url, editCode, content) (cont: DbContext): Story =
  let story = cont.Dbo.Stories.Create ()
  story.Url <- url
  story.EditCode <- editCode
  story.Content <- content
  cont.SubmitUpdates ()
  story
